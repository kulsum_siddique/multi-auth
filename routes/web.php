<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('auth.login');
});
// Auth::routes();
Route::middleware('admin')->group(function () {
    Route::get('/admin-home', function () {
        return view('admin-home');
    });  
});

Route::middleware('client')->group(function () {
    Route::get('/client-home', function () {
        return view('client-home');
    });
   
});

Route::middleware('production')->group(function () {
    Route::get('/production-home', function () {
        return view('production-home');
    });
   
});